# Hyperboliq - Assessment

Average Tile / Split RGBs, calculate distance between tiles and splits, and then replace the shortest distance split with corresponding tile

## Getting Started

### Prerequisites

What things you need to install to run the application

// Python 
Python 3.6+
PIP
Preferrably PyCharm

// Packages
imageio
os
cv2 (Open CV)
colormath
numpy

### Installing

Install the packages above, as well as the latest version of python / pip if not already installed

## Running the tests

Either run the application through PyCharm or via the cli with "Python main.py"


### And coding style tests

Followed python 3+ conventions for naming and comments

## Author 
Kevin Jacobs

## Last Updated
2021/03/02
