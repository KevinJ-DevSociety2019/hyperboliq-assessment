# Author Kevin Jacobs #
# Date 2021/03/02 #

# Needed packages
import imageio
import os
import cv2
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000
import numpy as np

# Global Variables
grid_locations = []

# Helper
def index_in_list(a_list, index):
    # Validate list
    if (isinstance(a_list, float)):
        return False
    return index < len(a_list)

# Step 2
def readFilesAndAverage(folder_path):
    # Initialize variables
    rgbAverages = []

    # Load images in path and calculate average rgb
    # Numpy is awesome!
    for file in os.listdir(folder_path):
        if file.endswith(".jpg"):
            try:
                rgbAverages.append(imageio.imread(folder_path + '/' + file).mean(axis=(0, 1), dtype=np.float64))
            except Exception as e:
                print("Oops, that file type shouldnt be here!")
                raise e

    return rgbAverages

# Step 3
def cutMainImage(image):
    # Initialize variables
    im              = cv2.imread(image)
    imgheight       = im.shape[0]
    imgwidth        = im.shape[1]
    M               = imgheight // 20
    N               = imgwidth // 20

    # This exception handling was done earlier, when I still had a bit more time
    # This function cuts the base image into 20x20 tiles, or 400 images
    try:
        for y in range(0, imgheight, M):
            for x in range(0, imgwidth, N):
                tiles = im[y:y + M, x:x + N]
                grid_locations.append({'x' : x, 'y': y})
                cv2.imwrite("./images/main-image/split-images/" + str(x) + '_' + str(y) + ".jpg", tiles)
    except Exception as e:
        print("Something went wrong when splitting the main image.")
        raise e

    # Find the average RGB for the split images created above
    try:
        averageGridRBG = readFilesAndAverage('./images/main-image/split-images')
    except Exception as e:
        print("Something went wrong when calculating the average rgb of the split images.")
        raise e

    return averageGridRBG

# Step 4
def calculateDistances(tiles, splits):
    # Initialize variables
    differences                 = []
    smallest_found_difference   = []
    tile_to_change              = []
    split_to_change             = []

    for tile, split in zip(tiles, splits):

        list_tile = tile.tolist()
        list_split = split.tolist()

        # Tile Color - Slight error handling, images may not always contain R G and B
        if(index_in_list(list_tile, 0) and index_in_list(list_tile, 1) and index_in_list(list_tile, 2)):
            tile_rgb = sRGBColor(list_tile[0], list_tile[1], list_tile[2])
        elif(index_in_list(list_tile, 0) and index_in_list(list_tile, 1) and not index_in_list(list_tile, 2)):
            tile_rgb = sRGBColor(list_tile[0], list_tile[1], 0)
        elif(index_in_list(list_tile, 0) and not index_in_list(list_tile, 1) and not index_in_list(list_tile, 2)):
            tile_rgb = sRGBColor(list_tile[0], 0, 0)

        # Split Color - Slight error handling, images may not always contain R G and B
        if (index_in_list(list_split, 0) and index_in_list(list_split, 1) and index_in_list(list_split, 2)):
            split_rgb = sRGBColor(list_split[0], list_split[1], list_split[2])
        elif (index_in_list(list_split, 0) and index_in_list(list_split, 1) and not index_in_list(list_split, 2)):
            split_rgb = sRGBColor(list_split[0], list_split[1], 0)
        elif (index_in_list(list_split, 0) and not index_in_list(list_split, 1) and not index_in_list(list_split, 2)):
            split_rgb = sRGBColor(list_split[0], 0, 0)

        # Convert from RGB to Lab Color Space
        color1_lab = convert_color(tile_rgb, LabColor)

        # Convert from RGB to Lab Color Space
        color2_lab = convert_color(split_rgb, LabColor)

        # Find the color distance - Please do check the helper function "delta_e_cie2000" here:
        # https://colour.readthedocs.io/en/develop/generated/colour.difference.delta_E_CIE2000.html
        delta_e = delta_e_cie2000(color1_lab, color2_lab)
        differences.append(delta_e)

        smallest_difference = min(differences)

        # This is here, as the loop is adding distances until the smallest distance is found
        # I dont break the loop once found, because we still need all distances/differences
        if (smallest_difference == delta_e):
            smallest_found_difference = smallest_difference
            tile_to_change = list_tile
            split_to_change = list_split

    to_return = {"smallest_difference": smallest_found_difference, "differences": differences,
                "tile_to_change" : tile_to_change, "split_to_change" : split_to_change}

    return to_return

# Step 5
def replaceTileWithSplit(difference_dict, tiles, splits):

    # Initialize variables
    baseimg                 = cv2.imread('./images/main-image/image_0001.jpg')
    tile_to_change          = difference_dict['tile_to_change']
    split_to_change         = difference_dict['split_to_change']
    tile_index              = 0
    split_index             = 0
    filename_tile           = ""

    # Map out index values
    for index, (tile, split) in enumerate(zip(tiles, splits)):

        list_tile = tile.tolist()
        list_split = split.tolist()
        if(tile_to_change == list_tile):
            tile_index = index
        if (split_to_change == list_split):
            split_index = index

    # Find the tile image
    for index, (filename) in enumerate(os.listdir("./images/tiles/rooster")):
        if index == tile_index:
            filename_tile = filename

    # Find the split image
    for index, (filename) in enumerate(os.listdir("./images/main-image/split-images")):
        if index == split_index:
            filename_split = filename

    # Final stretch - Find the location of the original split, find the corresponding tile, and save it on to the base
    # image
    try:

        # Load split
        tile_to_replace = cv2.imread('./images/main-image/split-images/' + filename_split)

        # Load tile
        split_to_replace = cv2.imread('./images/tiles/rooster/' + filename_tile)

        # Resize tile to fit split
        ht1, wt1 = tile_to_replace.shape[:2]
        split_to_replace = cv2.resize(split_to_replace, (ht1, wt1))

        # Load offset locations from global location
        x_offset_found = grid_locations[split_index]['x']
        y_offset_found = grid_locations[split_index]['y']

        # Load H/W of tile || This isnt needed as we could just specify 20,20, but I left it in too play around with
        # sizes
        h1, w1 = split_to_replace.shape[:2]

        # Insert tile on to base image
        baseimg[y_offset_found:y_offset_found + h1, x_offset_found:x_offset_found + w1] = split_to_replace

        # View the updated base image
        cv2.imshow('Easier to just view it than to try find it in the directory', baseimg)
        cv2.waitKey(0)

        # Save the updated base image
        cv2.imwrite(r'./images/main-image/image_0001.jpg', baseimg)
    except Exception as e:
        print('Saving the new image failed')
        raise e
    return True

# Step 1
def main():
    # Initialization successful
    print("We have lift off!")

    if __name__ == "__main__":

        # Step 2
        tile_averages = readFilesAndAverage('./images/tiles/rooster')

        if(tile_averages):

            # Step 3
            split_averages  = cutMainImage('./images/main-image/image_0001.jpg')

            print("Average RGB for each tile image: ")
            print(tile_averages)

            if (split_averages):

                print("Average RGB for each split image: ")
                print(split_averages)

                # Step 4
                difference_dict = calculateDistances(tile_averages, split_averages)

                if(difference_dict):

                    print("Differences between tiles and splits")
                    print(difference_dict['differences'])

                    # Step 5
                    final_step = replaceTileWithSplit(difference_dict, tile_averages,
                    split_averages)

                    if(final_step) :
                        print('The base image was successfully updated, exiting :)')
                        exit(1)
                    else :
                        print('Updating the base image failed - ToDo, add more exception handling so'
                              'that this response is more useful, exiting :(')
                        exit(1)

                else:
                    exit(1)
            else:
                exit(1)
        else:
            exit(1)
    else:
        exit(1)

# Main Entry #
main()
